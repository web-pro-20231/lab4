import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 1,
    email: 'icelnwza007@gmail.com',
    password: '1111',
    fullName: 'icelnwza',
    gender: 'male',
    roles: ['user']
})

  return { currentUser }
})
